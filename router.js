// -------------------------------------------------------------- //
// ----------------------< Маршрутизация >----------------------- //
// -------------------------------------------------------------- //

import Router from 'express'
import PostController from "./PostController.js"
import GetController from "./GetController.js"
import auth from "./Guards.js"

const router = new Router()

// -------------------------------------------------------------- //
//-----------------// Регистрация, авторизация //-----------------//
router.post('/auth', PostController.auth) // Авторизация
router.post('/registration', PostController.registration) // Регистрация пользователя
router.post('/logout', PostController.logout) // Выход
router.post('/checkToken', PostController.checkToken) // Проверка токена

// -------------------------------------------------------------- //
//---------------------// Получение данных //---------------------//
router.get('/getAllUsers', auth.isAuthenticated, GetController.getAllUsers) // Получить всех пользователей
router.get('/get-user/:id', auth.isAuthenticated, GetController.getUser) // Получить пользователя
router.get('/notifications', auth.isAuthenticated, GetController.getNotifications) // Получить уведомления
router.get('/getAmountNewNotifications', auth.isAuthenticated, GetController.getAmountNewNotifications) // Получить количество уведомлений
router.get('/getNewsFeed', auth.isAuthenticated, GetController.getNewsFeed) // Получить новостную ленту
router.get('/getAllShares', auth.isAuthenticated, GetController.getAllShares) // Получить все акции
router.get('/getQuotes', auth.isAuthenticated, GetController.getQuotes) // Получить котировки 
router.get('/getallQuotes', GetController.getallQuotes) // Получить все котировки 

// -------------------------------------------------------------- //
//---------------------// Добавление данных //--------------------//

// -------------------------------------------------------------- //
//----------------------// Удаление данных //---------------------//

export default router;