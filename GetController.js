// -------------------------------------------------------------- //
// --------< GET GET GET - контроллер. Обработка запроса >------- //
// -------------------------------------------------------------- //

import GetService from "./services/GetService.js";
import funcService from "./services/funcService.js";

class GetController {
    
    async getAllUsers(req, res) {
        try {
            console.log('Получение всех пользователей: ', req.query)
            const post = await GetService.getAllUsers(req.query)
            res.json(post)
        } catch (e) {
            console.log('\nERROR /getAllUsers: ', e, '\n')
            res.status(500).json(e)
        }
    }

    async getUser(req, res) {
        try {
            console.log('Получение пользователя: ', req.query)
            let list_of_required_keys = [ ['id_user', 'number'] ]
            let check = funcService.checking_for_undefined(req.query, list_of_required_keys)
            if(check != true)
                return res.status(401).json(funcService.err_response(check))
            const post = await GetService.getUser(req.query, req.params.id)
            res.json(post)
        } catch (e) {
            console.log('\nERROR /getUser: ', e, '\n')
            res.status(500).json(e)
        }
    }

    async getNotifications(req, res) {
        try {
            console.log('Получение уведомлений: ', req.query)
            let list_of_required_keys = [ ['id_user', 'number'], ['page', 'number'] ] // quantity, page
            let check = funcService.checking_for_undefined(req.query, list_of_required_keys)
            if(check != true)
                return res.status(401).json(funcService.err_response(check))
            const post = await GetService.getNotifications(req.query)
            res.json(post)
        } catch (e) {
            console.log('\nERROR /getNotifications: ', e, '\n')
            res.status(500).json(e)
        }
    }

    async getAmountNewNotifications(req, res) {
        try {
            console.log('Получение количества уведомлений: ', req.query)
            let list_of_required_keys = [ ['id_user', 'number'] ]
            let check = funcService.checking_for_undefined(req.query, list_of_required_keys)
            if(check != true)
                return res.status(401).json(funcService.err_response(check))
            const post = await GetService.getAmountNewNotifications(req.query)
            res.json(post)
        } catch (e) {
            console.log('\nERROR /getAmountNewNotifications: ', e, '\n')
            res.status(500).json(e)
        }
    }

    async getNewsFeed(req, res) {
        try {
            console.log('Получение новостной ленты: ', req.query)
            let list_of_required_keys = [ ['id_user', 'number'], ['page', 'number'] ]
            let check = funcService.checking_for_undefined(req.query, list_of_required_keys)
            if(check != true)
                return res.status(401).json(funcService.err_response(check))
            const post = await GetService.getNewsFeed(req.query)
            res.json(post)
        } catch (e) {
            console.log('\nERROR /getNewsFeed: ', e, '\n')
            res.status(500).json(e)
        }
    }

    async getAllShares(req, res) {
        try {
            const post = await GetService.getAllShares(req.query)
            res.json(post)
        } catch (e) {
            console.log('\nERROR /getAllShares: ', e, '\n')
            res.status(500).json(e)
        }
    }

    async getQuotes(req, res) {
        try {
            console.log('Получение котировок: ', req.query)
            let list_of_required_keys = [ ['id_user', 'number'], ['type', 'string'], ['identifier', 'number'] ]
            let check = funcService.checking_for_undefined(req.query, list_of_required_keys)
            if(check != true)
                return res.status(401).json(funcService.err_response(check))
            const post = await GetService.getQuotes(req.query)
            res.json(post)
        } catch (e) {
            console.log('\nERROR /getQuotes: ', e, '\n')
            res.status(500).json(e)
        }
    }

    async getallQuotes(req, res) {
        try {
            const post = await GetService.getallQuotes(req.query)
            res.json(post)
        } catch (e) {
            console.log('\nERROR /getallQuotes: ', e, '\n')
            res.status(500).json(e)
        }
    }


}

export default new GetController();