// -------------------------------------------------------------- //
// --------------< Настройка и запуск веб-сервера >-------------- //
// -------------------------------------------------------------- //
import express from 'express';
import router from "./router.js";

import funcService from "./services/funcService.js";
import SocketPost from "./services/SocketPostService.js";
import SqlService from "./services/sqlService.js";
import { createServer } from "http";
import { Server } from "socket.io";

const httpServer = createServer();
const io = new Server(httpServer, {
  // ...
});

const PORT = 5000;

var users = [] // здесь держу сокет-пользователей

const app = express()

app.use(express.json())
app.use(express.static('static'))

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS")
    res.header("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With")
    next()
})

app.use(express.json({ limit: '1mb' }));

// -------------------------------------------------------------- //
// ----------------</ Запуск веб-сокет сервера >----------------- //
// -------------------------------------------------------------- //

app.use('/api', router)

async function startApp() {
    try {
        app.listen(PORT, () => console.log('SERVER STARTED ON PORT ' + PORT))
    } catch (e) {
        console.log(e)
    }
}

startApp()

app.get('/', function (req, res) {
    res.send({ msg: 'hello, world!' });
});

app.get('/get_list_users_socket', function (req, res) {
    // Смотрю подключённых пользователей
    res.send(users);
});

// > > > > Взаимодействие с аналитическим модулем

app.get('/update_status_of_the_shares', function (req, res) {
    // Обновление акции! Уведомим всех, кто в онлайн
    for (let u = 0; u < users.length; u++) {
        for (let d = 0; d < users[u].token.length; d++) {
            socket.broadcast.to(users[u].token[d]).emit('res-update_status_shares', { msg: "Статус акции изменился", id_shares: req.query.id })
        }
    }
    res.send({ msg: 'success' });
});

app.get('/adding_quote', function (req, res) {
    // Обновление акции! Уведомим всех, кто в онлайн
    for (let u = 0; u < users.length; u++) {
        for (let d = 0; d < users[u].token.length; d++) {
            socket.broadcast.to(users[u].token[d]).emit('res-adding_quote', { id_shares: req.query.id, price_open: req.query.price_open, price_closing: req.query.price_closing })
        }
    }
    res.send({ msg: 'success' });
});

// > > > > Взаимодействие с аналитическим модулем

// -------------------------------------------------------------- //
// ------------------</ СОКЕТЫ СОКЕТЫ СОКЕТЫ >------------------- //
// -------------------------------------------------------------- //
io.on('connection', socket => {

    socket.join(socket.handshake.sessionID);

    // Присоединение к чату
    socket.on('new-user', user_id => {
        let stoping = false
        let index_user_id = 0
        for (let i = 0; i < users.length; i++) {
            if (user_id == users[i].id) {
                index_user_id = i
                stoping = true
                break
            }
        }
        if (stoping == false) {
            users.push({ id: user_id, token: [socket.id] })
        } else {
            users[index_user_id].token.push(socket.id)
        }
        // [{"id":1,"token":["---------1----------", "----------2---------"], ... }, {...}, {...}, ... ]

        get_info("SELECT * FROM `users` WHERE `id` = '" + user_id + "'", function (user) {
            if (user.length > 0) {
                delete user[0].password
                socket.broadcast.emit('user-connected', user[0])
                funcService.user_was_online(user_id) // Запишем контрольное время в онлайн:
                if (stoping == false) {
                    SqlService.post_info("INSERT INTO `logs`(`id_user`, `event`)VALUES('" + user_id + "', 'went_online')", function (resultat) { })
                }
            }
        })
    })


    // Отсоединение пользователя от чата
    socket.on('disconnect', () => {
        for (let i = 0; i < users.length; i++) {
            if (users[i] != undefined) {
                let length_token = Object.keys(users[i].token).length
                for (let j = 0; j < length_token; j++) {
                    if (users[i].token[j] == socket.id) {
                        out_user = users[i].id
                        funcService.user_was_online(out_user) // Запишем контрольное время в онлайн:
                        users[i].token = funcService.remove(users[i].token, j)
                        // Очистим, если больше нет записей, и запишем метку, что он вышел из онлайна
                        if (Object.keys(users[i].token).length == 0) {
                            SqlService.post_info("INSERT INTO `logs`(`id_user`, `event`)VALUES('" + out_user + "', 'went_offline')", function (resultat) { })
                            socket.broadcast.emit('user-disconnected', out_user)
                            users = funcService.remove(users, i)
                        }
                    }
                }
            }
        }
    })

    // Добавление новости
    socket.on('create-news', async data => {
        try {
            console.log('(socket) Добавление новости: ', data)
            let list_of_required_keys = [ ['id_user', 'number'], ['type', 'number'], ['identifier', 'string'], ['title', 'string'], ['description', 'string'], ['image', 'string'], ['status', 'string']  ]
            let check = funcService.checking_for_undefined(data, list_of_required_keys)
            if(check != true)
                return socket.emit('res-socket-error', funcService.err_response(check))
            const post = await SocketPost.CreateNews(data)
            if(post != false)
                socket.emit('res-create-news', { date: post })
            else
                socket.emit('res-socket-error', { msg: 'error', text: 'Новость не добавлена.' })
        } catch (e) {
            console.log('\nERROR (socket) create-news: ', e, '\n')
            socket.emit('res-socket-error', { msg: 'error', text: e })
        }
    })

})

httpServer.listen(3000);