// -------------------------------------------------------------- //
// ------< POST POST POST - контроллер. Обработка запроса >------ //
// -------------------------------------------------------------- //

import PostService from "./services/PostService.js";
import funcService from "./services/funcService.js";

class PostController {

    async auth(req, res) {
        try {
            console.log('Авторизация пользователя: ', req.body)
            let list_of_required_keys = [ ['password', 'string'], ['phone', 'string'] ]
            let check = funcService.checking_for_undefined(req.body, list_of_required_keys)
            if(check != true)
                return res.status(401).json(funcService.err_response(check))
            const post = await PostService.auth(req.body.phone, req.body.password, req.ip)
            if(post == false)
                return res.status(401).json(funcService.err_response('auth'))
            res.json(post)
        } catch (e) {
            console.log('\nERROR /auth: ', e, '\n')
            res.status(500).json(e)
        }
    }

    async registration(req, res) {
        try {
            // Добавить проверку на возможность регистрации. 
            console.log('Регистрация пользователя: ', req.body)
            let list_of_required_keys = [ ['name', 'string'], ['surname', 'string'], ['phone', 'string'], ['password', 'string'], ['company', 'number'] ]
            let check = funcService.checking_for_undefined(req.body, list_of_required_keys)
            if(check != true)
                return res.status(401).json(funcService.err_response(check))
            const post = await PostService.registration(req.body)
            if(post == false)
                return res.status(401).json(funcService.err_response('auth'))
            res.json(post)
        } catch (e) {
            console.log('\nERROR /registration: ', e, '\n')
            res.status(500).json(e)
        }
    }

    async logout(req, res) {
        try {
            console.log('Выход из аккаунта: ', req.body)
            let list_of_required_keys = [ ['id_user', 'number'], ['token', 'string'] ] // Добавить fcm_token
            let check = funcService.checking_for_undefined(req.body, list_of_required_keys)
            if(check != true)
                return res.status(401).json(funcService.err_response(check))
            const post = await PostService.logout(req.body)
            res.json(post)
        } catch (e) {
            console.log('\nERROR /logout: ', e, '\n')
            res.status(500).json(e)
        }
    }

    async checkToken(req, res) {
        try {
            console.log('Проверка токена: ', req.body)
            let list_of_required_keys = [ ['id_user', 'number'], ['token', 'string'] ]
            let check = funcService.checking_for_undefined(req.body, list_of_required_keys)
            if(check != true)
                return res.status(401).json(funcService.err_response(check))
            const post = await PostService.checkToken(req.body)
            res.json(post)
        } catch (e) {
            console.log('\nERROR /checkToken: ', e, '\n')
            res.status(500).json(e)
        }
    }

}


export default new PostController();
