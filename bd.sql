-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Сен 05 2021 г., 10:05
-- Версия сервера: 5.6.38
-- Версия PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `katirovki`
--

-- --------------------------------------------------------

--
-- Структура таблицы `briefcase`
--

CREATE TABLE `briefcase` (
  `id` int(12) NOT NULL,
  `id_user` int(12) NOT NULL,
  `type` varchar(64) NOT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `briefcase_contents`
--

CREATE TABLE `briefcase_contents` (
  `id` int(12) NOT NULL,
  `id_briefcase` int(12) NOT NULL,
  `type` varchar(64) NOT NULL,
  `identifier` int(12) NOT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `logs`
--

CREATE TABLE `logs` (
  `id` int(12) NOT NULL,
  `id_user` int(12) NOT NULL,
  `event` varchar(40) NOT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `news_feed`
--

CREATE TABLE `news_feed` (
  `id` int(12) NOT NULL,
  `type` varchar(24) NOT NULL,
  `identifier` int(12) NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `status` varchar(24) NOT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `notifications`
--

CREATE TABLE `notifications` (
  `id` int(12) NOT NULL,
  `id_user` int(12) NOT NULL,
  `id_action` int(12) NOT NULL,
  `text` text NOT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(24) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `quotes`
--

CREATE TABLE `quotes` (
  `id_quotes` int(12) NOT NULL,
  `type` varchar(64) NOT NULL,
  `identifier` int(12) NOT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `price_open` float NOT NULL,
  `price_closing` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `quotes`
--

INSERT INTO `quotes` (`id_quotes`, `type`, `identifier`, `time`, `price_open`, `price_closing`) VALUES
(1, 'shares', 1, '2021-09-05 09:45:21', 298.1, 0),
(2, 'shares', 1, '2021-09-05 09:45:31', 296, 295.4),
(3, 'shares', 1, '2021-09-05 09:45:39', 295.4, 298.9),
(4, 'shares', 1, '2021-09-05 09:45:48', 301.2, 303.9),
(5, 'shares', 1, '2021-09-05 09:45:57', 305.88, 304.01),
(6, 'shares', 1, '2021-09-05 09:46:05', 306.05, 314.2),
(7, 'shares', 1, '2021-09-05 09:46:14', 313.7, 311.9),
(8, 'shares', 1, '2021-09-05 09:46:23', 312.12, 312.88),
(9, 'shares', 2, '2021-09-05 09:46:49', 324, 324.8),
(10, 'shares', 2, '2021-09-05 09:47:53', 324.3, 324),
(11, 'shares', 2, '2021-09-05 09:48:10', 323.71, 327.1),
(12, 'shares', 2, '2021-09-05 09:48:20', 328.43, 328.3),
(13, 'shares', 2, '2021-09-05 09:48:30', 328.5, 328.85),
(14, 'shares', 2, '2021-09-05 09:48:40', 328.87, 332.55),
(15, 'shares', 2, '2021-09-05 09:48:53', 334.67, 334.11),
(16, 'shares', 2, '2021-09-05 09:49:04', 329.47, 329.9),
(17, 'shares', 3, '2021-09-05 09:50:09', 51.95, 52.245),
(18, 'shares', 3, '2021-09-05 09:50:18', 52.245, 51.895),
(19, 'shares', 3, '2021-09-05 09:50:29', 50.97, 52.4),
(20, 'shares', 3, '2021-09-05 09:50:42', 51.895, 54.695),
(21, 'shares', 3, '2021-09-05 09:50:53', 52.4, 56.95),
(22, 'shares', 3, '2021-09-05 09:51:02', 53.5, 53.95),
(23, 'shares', 3, '2021-09-05 09:51:13', 54.695, 54),
(24, 'shares', 3, '2021-09-05 09:51:24', 56.95, 55.2),
(25, 'shares', 4, '2021-09-05 09:51:44', 238.2, 242.1),
(26, 'shares', 4, '2021-09-05 09:51:52', 244, 241.1),
(27, 'shares', 4, '2021-09-05 09:52:57', 242.02, 250.3),
(28, 'shares', 4, '2021-09-05 09:53:06', 246.82, 248.6),
(29, 'shares', 4, '2021-09-05 09:53:18', 252, 247.5),
(30, 'shares', 4, '2021-09-05 09:53:27', 248.84, 246),
(31, 'shares', 4, '2021-09-05 09:53:35', 246, 244.5),
(32, 'shares', 4, '2021-09-05 09:53:44', 244.5, 245.5),
(33, 'shares', 5, '2021-09-05 09:55:02', 179.79, 179.5),
(34, 'shares', 5, '2021-09-05 09:55:12', 179.5, 183.1),
(35, 'shares', 5, '2021-09-05 10:02:13', 180.1, 182),
(36, 'shares', 5, '2021-09-05 10:02:28', 181.5, 178.2),
(37, 'shares', 5, '2021-09-05 10:02:36', 182.9, 177.7),
(38, 'shares', 5, '2021-09-05 10:02:51', 184.99, 187.3),
(39, 'shares', 5, '2021-09-05 10:03:01', 187.3, 191.9),
(40, 'shares', 5, '2021-09-05 10:03:11', 191.4, 191),
(41, 'shares', 6, '2021-09-05 10:03:29', 189.79, 189.5),
(42, 'shares', 6, '2021-09-05 10:03:37', 189.5, 192.9),
(43, 'shares', 6, '2021-09-05 10:03:49', 190.1, 188.9),
(44, 'shares', 6, '2021-09-05 10:03:58', 191.5, 189.99),
(45, 'shares', 6, '2021-09-05 10:04:10', 192.9, 191.4),
(46, 'shares', 6, '2021-09-05 10:04:19', 189.99, 191.4),
(47, 'shares', 6, '2021-09-05 10:04:29', 190.3, 189.99),
(48, 'shares', 6, '2021-09-05 10:04:38', 191.4, 190.2);

-- --------------------------------------------------------

--
-- Структура таблицы `registration`
--

CREATE TABLE `registration` (
  `id` int(12) NOT NULL,
  `name` text NOT NULL,
  `surname` text NOT NULL,
  `patronymic` text NOT NULL,
  `phone` varchar(24) NOT NULL,
  `password` text NOT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `code` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `shares`
--

CREATE TABLE `shares` (
  `identifier` int(11) NOT NULL,
  `title` text NOT NULL,
  `reduction` text NOT NULL,
  `abbreviation` varchar(24) NOT NULL,
  `type` varchar(24) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shares`
--

INSERT INTO `shares` (`identifier`, `title`, `reduction`, `abbreviation`, `type`) VALUES
(1, 'Публичное акционерное общество \"Газпром\", акция обыкновенная (GAZP)', 'ГАЗПРОМ ао', 'GAZP', ''),
(2, 'Публичное акционерное общество \"Сбербанк России\", акция обыкновенная (SBER)', 'Сбербанк', 'SBER', ''),
(3, 'Международная компания публичное акционерное общество \"Объединённая Компания \"РУСАЛ\"\", акция обыкновенная (RUAL)', 'РУСАЛ ао', 'RUAL', ''),
(4, 'Публичное акционерное общество \"Новолипецкий металлургический комбинат\", акция обыкновенная (NLMK)', 'НЛМК ао', 'NLMK', ''),
(5, 'Публичное акционерное общество \"Московская Биржа ММВБ-РТС\", акция обыкновенная (MOEX)', 'МосБиржа', 'MOEX', ''),
(6, 'Публичное акционерное общество \"Придумка\", акция обыкновенная (TEST)', 'Придумка', 'TEST', '');

-- --------------------------------------------------------

--
-- Структура таблицы `token`
--

CREATE TABLE `token` (
  `id` int(12) NOT NULL,
  `id_user` int(12) NOT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip` varchar(20) NOT NULL,
  `token` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id_user` int(12) NOT NULL,
  `name` text NOT NULL,
  `surname` text NOT NULL,
  `patronymic` text NOT NULL,
  `phone` varchar(24) NOT NULL,
  `password` text NOT NULL,
  `lvl_user` int(1) NOT NULL,
  `online` varchar(24) NOT NULL,
  `registration` varchar(24) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `briefcase`
--
ALTER TABLE `briefcase`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `briefcase_contents`
--
ALTER TABLE `briefcase_contents`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `news_feed`
--
ALTER TABLE `news_feed`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `quotes`
--
ALTER TABLE `quotes`
  ADD PRIMARY KEY (`id_quotes`);

--
-- Индексы таблицы `registration`
--
ALTER TABLE `registration`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `shares`
--
ALTER TABLE `shares`
  ADD PRIMARY KEY (`identifier`);

--
-- Индексы таблицы `token`
--
ALTER TABLE `token`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `briefcase`
--
ALTER TABLE `briefcase`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `briefcase_contents`
--
ALTER TABLE `briefcase_contents`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `news_feed`
--
ALTER TABLE `news_feed`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `quotes`
--
ALTER TABLE `quotes`
  MODIFY `id_quotes` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT для таблицы `registration`
--
ALTER TABLE `registration`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `shares`
--
ALTER TABLE `shares`
  MODIFY `identifier` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `token`
--
ALTER TABLE `token`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(12) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
