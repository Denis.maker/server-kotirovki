// -------------------------------------------------------------- //
// --------< POST POST POST - сервис. Подготовка ответа >-------- //
// -------------------------------------------------------------- //

import SqlService from "./sqlService.js";
import funcService from "./funcService.js";

class SocketPost {

    async CreateNews(data){
        try {
            console.log('Сейчас я в CreateNews')
            let id_new_news = SqlService.post_info("INSERT INTO `news_feed`(`type`, `identifier`, `title`, `description`, `image`, `status`)VALUES('"+data.type+"', '"+data.identifier+"', '"+data.title+"', '"+data.description+"', '"+data.image+"', '"+data.status+"')");
            return funcService.succes_response({id_new_news: id_new_news})
        } catch (e) {
            console.log('Произошла ошибка, но сервер не лёг!')
            return false
        }
    }

}


export default new SocketPost();
