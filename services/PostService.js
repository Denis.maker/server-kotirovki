// -------------------------------------------------------------- //
// --------< POST POST POST - сервис. Подготовка ответа >-------- //
// -------------------------------------------------------------- //

import SqlService from "./sqlService.js";
import funcService from "./funcService.js";
import CryptoJS from "crypto-js";

class PostService {

    async auth(phone_numb, pass, ip){
        try {
            console.log('Сейчас я в PostService, ip: ', ip)
            let password = CryptoJS.MD5(pass);
            let phone = await funcService.phoneNumberOptimization(phone_numb)
            let res = await SqlService.get_data('SELECT * FROM users WHERE phone = "'+phone+'" AND password = "'+password+'"')
            if (res.length > 0) {
                let rand_value = Math.floor(Math.random() * (9999 - 1000 + 1)) + 1000;
                let token = CryptoJS.MD5(rand_value.toString());
                SqlService.post_info("INSERT INTO `token`(`id_user`, `ip`, `token`)VALUES('"+res[0].id+"', '"+ip+"', '"+token+"')");
                delete res[0].password
                res[0].role = funcService.defineTheRole(res[0].lvl_user)
                delete res[0].lvl_user
                res[0].token = token.toString()
                return res[0]
            } else
            return false
        } catch (e) {
            console.log('Произошла ошибка, но сервер не лёг!')
            return false
        }
    }

    async registration(data) {
        data.phone = await funcService.phoneNumberOptimization(data.phone)
        let result = await SqlService.get_data("SELECT * FROM `user` WHERE `phone` = '" + data.phone + "' ")
        if (result.length == 0)
            return funcService.err_response('number_already_exists')
        result = await SqlService.get_data("SELECT * FROM `registration` WHERE `phone` = '" + data.phone + "' ")
        if (result.length == 0)
            return funcService.err_response('phone_is_already_taken')
        let code = Math.floor(Math.random() * (9999 - 1000 + 1)) + 1000;
        SqlService.post_info("INSERT INTO `registration`(`name`, `surname`, `patronymic`, `phone`, `password`, `code`)VALUES('" + data.name + "', '" + data.surname + "', '" + data.patronymic + "', '" + data.phone + "', '" + password + "', '"+code+"')")
        // Надо отправить сообщение на телефон
        let sms_text = 'api.s-k56.ru/verification-phone?phone='+data.phone+'&code='+code
        // Отправить код ссылку на номер телефона!
        return funcService.succes_response('successful_registration')
    }

    async logout(data) {
        SqlService.get_data("DELETE FROM `token` WHERE `id_user` = '" + data['id_user'] + "' AND `token` = '" + data['token'] + "'")
        return funcService.succes_response(0)
    }

    async checkToken(data) {
        let result = SqlService.get_data("SELECT * FROM `token` WHERE `id_user` = '" + data['id_user'] + "' AND `token` = '" + data['token'] + "'")
        if (result == 0)
            return { msg: "token_not_found" }
        else
            return funcService.succes_response(0)
    }

}


export default new PostService();
