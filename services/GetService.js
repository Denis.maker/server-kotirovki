// -------------------------------------------------------------- //
// ----------< GET GET GET - сервис. Подготовка ответа >--------- //
// -------------------------------------------------------------- //

import SqlService from "./sqlService.js";
import funcService from "./funcService.js";

class GetService {

    async getAllUsers(data){
        if(await funcService.check_for_admin(data.id_user)){
            let res = await SqlService.get_data('SELECT * FROM `users`')
            for (let i = 0; i < res.length; i++) {
                res[i].role = funcService.defineTheRole(res[i].lvl_user)
                delete res[i].lvl_user
            }
            return res
        } else
            return funcService.err_response('access_is_denied')
    }

    async getUser(data, get_id){
        if(await funcService.check_for_admin(data.id_user)){
            let res = await SqlService.get_data('SELECT * FROM `users` WHERE `id` = "'+get_id+'"')
            res[0].role = funcService.defineTheRole(res[i].lvl_user)
            delete res[0].lvl_user
            delete res[0].password
            return res[0]
        } else
            return funcService.err_response('access_is_denied')
    }

    async getNotifications(data){
        let body_page
        if(data['page'] == undefined){
            body_page = 1
        } else {
            body_page = data['page']
        }
        let per_page = 30 // последующие выводы такие
        if(data['quantity'] != undefined && Math.abs(data['quantity']) < 300)
            per_page = Math.abs(data['quantity'])
        let start_sample = per_page * (body_page - 1)
        return await SqlService.get_data('SELECT * FROM `notifications` WHERE `id_recipient` = ' + data['id_user'] + ' ORDER BY id DESC LIMIT ' + start_sample + ', ' + per_page)
    }

    async getAmountNewNotifications(data){
        res = await SqlService.get_data('SELECT COUNT(*) FROM `notifications` WHERE `id_recipient` = ' + data['id_user'] + ' AND `status` = "unread"')
        console.log('количество новых уведомлений: ', res)
        return res
    }

    async getNewsFeed(data) {
        let body_page
        if(data['page'] == undefined){
            body_page = 1
        } else {
            body_page = data['page']
        }
        let per_page = 30 // последующие выводы такие
        if(data['quantity'] != undefined && Math.abs(data['quantity']) < 300)
            per_page = Math.abs(data['quantity'])
        let start_sample = per_page * (body_page - 1)
        return await SqlService.get_data('SELECT * FROM `news_feed`')
        // Смотрим портфель пользователя
        let res = await SqlService.get_data('SELECT * FROM `news_feed` WHERE `id_user` = ' + data['id_user'])
        if(res.length == 0)
            return []
        let array_id = '('
        for(let i = 0; i < res.length; i++)
            array_id += res[i].id_user
        array_id += ')'
        // Отправлем человеку новости только те новости, акциями которых он обладает
        return await SqlService.get_data('SELECT * FROM `news_feed` WHERE `identifier` = ' + array_id + ' ORDER BY id DESC LIMIT ' + start_sample + ', ' + per_page)
    }

    async getAllShares(data) {
        let res = await SqlService.get_data('SELECT * FROM `shares`')
        for(let i = 0; i < shares.length; i++) {
            let quot = await SqlService.get_data('SELECT * FROM `quotes` WHERE `id_quotes` = ' + res[i].identifier + ' ORDER BY id DESC LIMIT 1')
            res[i].price_open = quot[0].price_open
            res[i].price_closing = quot[0].price_closing
        }
        return res
    }

    async getQuotes(data) {
        return await SqlService.get_data('SELECT * FROM `quotes` WHERE `identifier` = ' + data.identifier + ' AND `type` = "'+data.type+'"')
    }

    async getallQuotes(data) {
        return await SqlService.get_data('SELECT * FROM `quotes`')
    }
    
}

export default new GetService();