// -------------------------------------------------------------- //
// --------------------< Утилитные функции >--------------------- //
// -------------------------------------------------------------- //

import SqlService from "./sqlService.js";

class funcService {
    
    checking_for_undefined(obj, arr_name){
        // Проверяем на пустые или некорректные данные (Проверяем наличие всех параметров и тип данных)
        for (let i = 0, len = arr_name.length; i < len; ++i) {
            if((!(arr_name[i][0] in obj))){
                console.log('Неполные данные, возвращаю ошибку. Не вижу ', arr_name[i][0])
                return 'Неполные данные, не вижу свойства ' + arr_name[i][0]
            }
            // string // number // object // function // undefined // boolean //
            if(typeof obj[arr_name[i][0]] != arr_name[i][1] && arr_name[i][1] != '') {
                if(arr_name[i][1] == 'number' && !isNaN(obj[arr_name[i][0]]))
                    continue
                console.log('Некорректный тип данных. Сделайте свойство ', arr_name[i][0], ': ', arr_name[i][1])
                return 'Некорректный тип данных. Сделайте свойство ' + arr_name[i][0] + ': ' + arr_name[i][1]
            }
        }
        return true
    }

    err_response(err){
        // Ответы сервера ошибкой
        let text = ''
        switch(err){
            case 0: text = 'Пустые данные'; break;
            case 'access_is_denied': text = 'Недостаточно прав'; break;
            case 'auth': text = 'Некорректный логин или пароль'; break;
            case 'number_already_exists': text = 'Номер уже существует в системе'; break;
            case 'phone_is_already_taken': text = 'Регистрационные данные уже приняты'; break;
            default: text = err; break;
        }
        console.log('> > > ОШИБКА: ' + text)
        return { msg: 'error', text: text}
    }
    
    succes_response(meaning){
        // Успешные ответы сервера
        let text = ''
        switch(meaning) {
            case 0: text = 'Успешно!'; break;
            case 'successful_registration': text = 'Регистрационные данные приняты. Ожидается подтверждение номера телефона.'; break;
            case 'user_registr_success': text = 'Успешная регистрация! Ожидаем верификации пользователя'; break;
            case 'log_add_success': text = 'Лог успешно добавлен'; break;
            default: text = meaning; break;
        }
        console.log('> > > УСПЕШНО: ' + text)
        return { msg: 'success', text: text}
    }


    phoneNumberOptimization(phone) {
        // Оптимизация номера
        try {
            if(phone.charAt(0) == 7)
                phone = phone.replace('7','8');
            phone = phone.replace("+7", "8");
            phone = phone.replace("-", "");
            phone = phone.replace("-", "");
            phone = phone.replace(" ", "");
            phone = phone.replace(" ", "");
            return phone;
        } catch (e) {
            console.log(e)
        }
    }

    defineTheRole(role){
        // Определение роли пользователя
        if(role == 2)
            return 'master'
        if(role == 3)
            return 'admin'
        if(role == -1)
            return 'blocked'
        return 'user'
    }

    async check_for_moderator(id){
        // Проверка на модератора
        try {
            let res = await SqlService.get_data('SELECT * FROM users WHERE id = "'+id+'"')
            if (res[0].lvl_user != undefined && res[0].lvl_user > 1)
                return true;
            return false;
        } catch (e) {
            console.log(e)
            return false;
        }
    }

    async check_for_admin(id){
        // Проверка на администратора
        try {
            let res = await SqlService.get_data('SELECT * FROM users WHERE id = "'+id+'"')
            if (res[0].lvl_user != undefined && res[0].lvl_user == 3)
                return true;
            return false;
        } catch (e) {
            console.log(e)
            return false;
        }
    }

    async get_ip(req) {
        // Получение ip
        let ip = req.headers['x-forwarded-for'] || 
        req.connection.remoteAddress || 
        req.socket.remoteAddress ||
        req.connection.socket.remoteAddress;
        console.log(ip);
        return ip
    }

    async user_was_online(id_user) {
        // Фиксируем последнее время в онлайн
        try {
            if(id_user != undefined && id_user > 0){
                timeInMs = Date.now() / 1000;
                post_info("UPDATE users SET `online` = '" + timeInMs + "' WHERE `id` = '" + id_user + "' ", function (resultat) { });
            } else
                console.log('ошибка записи последнего времени в онлайн. Некорректные данные')
        } catch (e) {
            console.log(e)
        }
    }

    async remove(arr, indexes) {
        try {
            var arrayOfIndexes = [].slice.call(arguments, 1);
            return arr.filter(function (item, index) {
                return arrayOfIndexes.indexOf(index) == -1;
            });
        } catch (e) {
            console.log(e)
        }
    }

}

export default new funcService();