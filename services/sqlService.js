// -------------------------------------------------------------- //
// ---------------< Подключение и работа с Mysql >--------------- //
// -------------------------------------------------------------- //

import mysql from "mysql2";
const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  database: 'katirovki',
  password: ""
});

connection.connect(error => {
  if (error) throw error;
  console.log("Successfully connected to the database.");
});

class SqlService {

    async post_info(sql) {
        return new Promise(function (resolve, reject) {
          connection.query(sql, function (err, results) {
            if (err) {
              console.log('error sql: ', sql);
              throw err;
            }
            return resolve(results.insertId);
          })
        })
    }
    
    async get_data(sql) {
        return new Promise(function (resolve, reject) {
          connection.query(sql, function (err, results) {
              if (err) {
              console.log('error sql: ', sql);
              throw err;
              }
              return resolve(results);
          })
        })
    }
    
}

export default new SqlService();
