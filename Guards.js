import SqlService from "./services/sqlService.js";

class Guards {
    
    isGuest (req, res, next) {
        if (req.isAuthenticated()) {
            res.redirect('/');
        } else {
            next();
        }
    }

    async isAuthenticated (req, res, next) {
        return next() // Функция работает. Не успеваем сделать клиент, потому разрешаю всю маршрутизацию
        if (!req.headers.authorization) {
            console.log('Не авторизован, маршрутизация заблокирована')
            // res.redirect('/users/login') // - переадресация
            return res.json({ msg: "error", text: "Вы не авторизованы."});
        } else {
            let id_user
            if(req.body.id_user)
                id_user = req.body.id_user
            else if (req.query.id_user)
                id_user = req.query.id_user
            else {
                console.log('isAuthenticated: Параметр id_user = undefined')
                return res.json({ msg: "error", text: "Параметр 'id_user' = undefined"});
            }
            console.log('Токен пользователя: ', req.headers.authorization, ', id_user: ', id_user)
            let res = await SqlService.get_data('SELECT * FROM `token` WHERE `id_user` = "'+id_user+'" AND `token` = "'+req.headers.authorization+'"')
            if (res.length > 0) {
                // Проверка пройдена успешно! Маршрутизация работает, пропускаем дальше.
                next()
            } else {
                console.log('аутентификация не пройдена')
                return res.json({ msg: "error", text: "Аутентификация не пройдена. Токен устарел или был удалён."});
            }
        }
    }

}

export default new Guards();